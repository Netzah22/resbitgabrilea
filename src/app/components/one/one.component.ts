import { Component, OnInit, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/common';
import { ZoomMtg } from '@zoomus/websdk';



@Component({
  selector: 'app-one',
  templateUrl: './one.component.html',
  styleUrls: ['./one.component.css']
})
export class OneComponent implements OnInit {

  signatureEndpoint = 'http://localhost:4000'
  sdkKey = '0yAbS7bQt1S7pPmoU4iwI1Qytxc8lsXcAWWj'
  meetingNumber = '8343783739'
  role = 0
  leaveUrl = 'http://localhost:4200'
  userName = 'Netzah'
  userEmail = ''
  passWord = 'HolaPuerco'
  registrantToken = ''

  constructor(public httpClient: HttpClient, @Inject(DOCUMENT) document) { }

  ngOnInit(): void {
  }

  // getSignature() {
  //   ZoomMtg.setZoomJSLib('https://source.zoom.us/2.3.0/lib', '/av');
  //   ZoomMtg.preLoadWasm();
  //   ZoomMtg.prepareWebSDK();
  //   ZoomMtg.i18n.load('en-US');
  //   ZoomMtg.i18n.reload('en-US');
  //   this.httpClient.post(this.signatureEndpoint, {
	//     meetingNumber: this.meetingNumber,
	//     role: this.role
  //   }).toPromise().then((data: any) => {
  //     if(data.signature) {
  //       console.log(data.signature)
  //       this.startMeeting(data.signature)
  //     } else {
  //       console.log(data)
  //     }
  //   }).catch((error) => {
  //     console.log(error)
  //   })
  // }

  // startMeeting(signature) {

  //   document.getElementById('zmmtg-root').style.display = 'block'

  //   ZoomMtg.init({
  //     leaveUrl: this.leaveUrl,
  //     success: (success) => {
  //       console.log(success)
  //       ZoomMtg.join({
  //         signature: signature,
  //         meetingNumber: this.meetingNumber,
  //         userName: this.userName,
  //         sdkKey: this.sdkKey,
  //         userEmail: this.userEmail,
  //         passWord: this.passWord,
  //         tk: this.registrantToken,
  //         success: (success) => {
  //           console.log(success)
  //         },
  //         error: (error) => {
  //           console.log(error)
  //         }
  //       })
  //     },
  //     error: (error) => {
  //       console.log(error)
  //     }
  //   })
  // }

}
